Link catre github unde este incarcat proiectul: https://github.com/Covva23/Licenta

Pasii pentru rularea proiectului sunt descrisi in fisierul README.md din proiectul de pe github.

Pasii sunt urmatorii:

Clonarea proiectului (git clone) sau se descarca arhiva .zip

Deschiderea proiectului in Android Studio File->Open->locatia proiectului

Se apasa make project

In device manager se creeaza un nou dispozitiv virtual
Se porneste dispozitivul virtual

Se apasa Run "app" sau se apasa Shift+F10

Aplicatia va porni si se va putea incerca aplicatia

Pentru rularea pe device mobil:

Se conecteaza telefonul la laptop

Din Setari dezvoltator din telefon se bifeaza USB debugging

In device manager la physical va aparea noul dispozitiv

Se apasa run "app" sau se apasa Shift+F10

Aplicatia va fi rulata pe dispozitivul fizic
